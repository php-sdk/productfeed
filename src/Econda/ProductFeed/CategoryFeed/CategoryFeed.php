<?php

namespace Econda\ProductFeed\CategoryFeed;

use Econda\ProductFeed\Description\Field;
use Econda\ProductFeed\CategoryFeed\Writer\CsvWriter;
use Econda\ProductFeed\AbstractFeed;

/**
 * @property CategoryReaderInterface $reader
 * @property string $readerClassName
 * @author cluetjen
 *
 */
class CategoryFeed extends AbstractFeed {

    /**
     * Instance of reader class
     * @var CategoryFeedWriterInterface
     */
    protected $reader;

    /**
     * Class name of reader class. We'll use this class name to create an instance if needed
     * @var unknown
     */
    protected $readerClassName;

    public function __construct($arguments = null) {
        $this->writer = new CsvWriter();

        parent::__construct($arguments);

        $this->fields = array(
            new Field(array('name' => 'ID', 'usage' => Field::USAGE_CATEGORY_ID)),
            new Field(array('name' => 'ParentID', 'usage' => Field::USAGE_CATEGORY_PARENT)),
            new Field(array('name' => 'Name', 'usage' => Field::USAGE_CATEGORY_NAME)),
        );
    }

    public function init() {
        $this->writer->inputCharset = $this->catalogInfo->charset;
    }

    public function getFields() {
        $allFields = $this->fields;
        foreach ($this->config->additionalLanguages as $languageKey) {
            $allFields[] = new Field(array(
                'name' => 'Name_' . $languageKey,
                'usage' => Field::USAGE_CATEGORY_NAME,
                'variation' => $languageKey
            ));
        }
        return $allFields;
    }

    public function writeAll() {
        $writer = $this->getWriter();
        $reader = $this->getReader();

        $writer->additionalLanguages = $this->config->additionalLanguages;

        $writer->writeHeader();
        while ($current = $reader->getNextCategory()) {
            $writer->writeCategory($reader->read($current));
        }
        $writer->writeFooter();
    }

    /**
     * Add category item to feed
     *
     * @param Category $category
     * @return \Econda\ProductFeed\CategoryFeed\CategoryFeed
     */
    protected function writeCategory(Category $category) {
        $this->writer->writeCategory($category);
        return $this;
    }

    public function getType() {
        return self::TYPE_CATEGORY_TREE;
    }

    public function getReader() {
        if (!isset($this->reader) && !empty($this->readerClassName)) {
            $className = $this->readerClassName;
            $this->reader = new $className();
            $this->initReader();
        }
        return $this->reader;
    }

    protected function initReader() {
        $r = $this->reader;/** @var CategoryListReaderInterface */
        $r->setConfig($this->config);
        $r->setCatalogInfo($this->catalogInfo);
        $r->init();
    }

}
