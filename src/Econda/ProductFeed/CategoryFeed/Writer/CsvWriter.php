<?php
namespace Econda\ProductFeed\CategoryFeed\Writer;

use Econda\ProductFeed\Catalog\CatalogInfo;

use Econda\ProductFeed\CategoryFeed\Category;
use Econda\ProductFeed\CategoryFeed\Writer\CategoryFeedWriterInterface;
use Econda\ProductFeed\Writer\AbstractCsvWriter;

/**
 * This writer knows how to write categories as CSV.
 * 
 * @property string[] $additionalLanguages
 * 
 * @author cluetjen
 */
class CsvWriter extends AbstractCsvWriter implements CategoryFeedWriterInterface
{
	protected $additionalLanguages;
	
	public function __construct($arguments=null)
	{
		// set default values
		$this->additionalLanguages = array();
		
		parent::__construct($arguments);
	}
	
	public function writeHeader()
	{
		$fields = array(
			'ID',
			'ParentID',
			'Name'
		);
		foreach($this->additionalLanguages as $languageKey) {
			$fields[] = 'Name_' . $languageKey;
		}
		$this->writeNamedColumnsHeader($fields);
	}
	
	public function writeCategory(Category $category)
	{
		$data = array(
			$category->getId(),
			$category->getParentId(),
			$category->getName()
		);
		foreach($category->getAdditionalLanguages() as $lang => $categoryName) {
			$data[] = $categoryName;
		}
		
		$this->writeRecord($data);
	}
	
	public function writeFooter()
	{
		// nothing to do for CSV output
	}
}