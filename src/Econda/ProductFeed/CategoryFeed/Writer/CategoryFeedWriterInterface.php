<?php
namespace Econda\ProductFeed\CategoryFeed\Writer;

use Econda\ProductFeed\Catalog\CatalogInfo;

use Econda\ProductFeed\CategoryFeed\Category;

/**
 * Defines a writer that can be used with a category feed object
 */
interface CategoryFeedWriterInterface
{
	public function open($uri=null);
	public function writeHeader();
	public function writeCategory(Category $category);
	public function writeFooter();
	public function close();
}