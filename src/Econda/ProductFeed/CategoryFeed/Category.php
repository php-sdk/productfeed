<?php
namespace Econda\ProductFeed\CategoryFeed;

use Econda\Util\BaseObject;

/**
 * A single shop category
 */
class Category extends BaseObject
{
	/**
	 * Category Id
	 * @var string
	 */
	protected $id;
	
	/**
	 * parent category id
	 * @var string
	 */
	protected $parentId;
	
	/**
	 * Category name in default language
	 * @var string
	 */
	protected $name;
	
	/**
	 * Array of additional languages
	 * 'language_key' => 'category name'
	 * @var array
	 */
	protected $additionalLanguages = array();

	/**
	 * Add additional language value to category
	 * 
	 * @param string $key
	 * @param string $name
	 * @return \Econda\ProductFeed\CategoryFeed\Category
	 */
	public function addAdditionalLanguage($key, $name)
	{
		$this->additionalLanguages[$key] = $name;
		return $this;
	}
}