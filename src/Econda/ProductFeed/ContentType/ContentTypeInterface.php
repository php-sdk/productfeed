<?php
namespace Econda\ProductFeed\ContentType;

/**
 * This interface defines must have properties and functions for all content type options classes
 * 
 * @author cluetjen
 */
interface ContentTypeInterface
{
	/**
	 * Must return the name of content type
	 * @return string
	 */
	public function getName();
	
	/**
	 * Get all format options as assoc array
	 * @return array
	 */
	public function getOptions();
	
	/**
	 * To string conversion must be supported for html renderer
	 * @return string
	 */
	public function __toString();
}