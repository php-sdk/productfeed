<?php
namespace Econda\ProductFeed\ContentType;

use Econda\Util\BaseObject;

/**
 * Options info for CSV files
 * 
 * @property string $encoding
 * @property string $delimiter
 * @property string $enclosure
 * @property bool   $firstLineContainsColumnNames
 * 
 * @author cluetjen
 */
class Csv extends BaseObject implements ContentTypeInterface
{
	const NAME = "text/csv";
	
	/**
	 * Text encoding
	 * @var string
	 */
	protected $encoding = "UTF-8";
	
	/**
	 * Character used to separate fields
	 * @var string
	 */
	protected $delimiter = "|";
	
	/**
	 * Character used to enclose field values
	 * @var string
	 */
	protected $enclosure = "\"";
	
	/**
	 * True if first line contains column names
	 * @var bool
	 */
	protected $firstLineContainsColumnNames = true;
	
	/**
	 * Name of content type
	 * @return string
	 */
	public function getName()
	{
		return self::NAME;
	}
	
	public function setEncoding($encoding)
	{
		$this->encoding = strtoupper($encoding);
		return $this;
	}
	
	public function getOptions()
	{
		return array(
			'encoding' => $this->encoding,
			'delimiter' => $this->delimiter,
			'enclosure' => $this->enclosure,
			'firstLineContainsColumnNames' => $this->firstLineContainsColumnNames
		);
	}
	
	public function __toString()
	{
		$html = self::NAME . " Options: ";
		foreach($this->getOptions() as $key => $value) {
			$html.= $value . "($key) ";
		}

		return $html;
	}
}