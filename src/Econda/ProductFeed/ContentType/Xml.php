<?php
namespace Econda\ProductFeed\ContentType;

/**
 * Options class for XML files.
 * 
 * @author cluetjen
 */
class Xml implements ContentType
{
	const NAME = "application/xml";
	
	/**
	 * Name of content type
	 * @return string
	 */
	public function getName()
	{
		return self::NAME;
	}
	
	/**
	 * Get array of xml options. 
	 * There are no options available for XML, so this function returns an empty array.
	 * 
	 * (non-PHPdoc)
	 * @see \Econda\ContentType\OptionsInterface::getOptions()
	 */
	public function getOptions()
	{
		return array();
	}
}