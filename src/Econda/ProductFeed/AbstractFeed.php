<?php
namespace Econda\ProductFeed;

use Econda\ProductFeed\ContentType\ContentTypeInterface;
use Econda\Util\BaseObject;
use Econda\ProductFeed\Exception\RuntimeException;

/**
 * Represents a single datasource (file or http uri) where we can retrieve data (products, categories).
 * 
 * @property \Econda\ProductFeed\Writer\AbstractWriter $writer;
 * @property string $defaultLanguage;
 * @property array $additionallanguages;
 * 
 * @author cluetjen
 *
 */
abstract class AbstractFeed extends BaseObject
{
	protected $config;
	protected $catalogInfo;
	
	/**
	 * instance of writer class, knows how to write objects to output format
	 * @var unknown
	 */
	protected $writer;
	
	/**
	 * Absolute or relative uri of datasource. URIs must not contain username and password!
	 * Relative uris will be resolved realtie to uri feed description (xml)
	 * 
	 * @var string
	 */
	protected $uri;
	
	const TYPE_PRODUCT_LIST = 'product-list';
	const TYPE_CATEGORY_TREE = 'category-tree';
	
	const CONTENT_TYPE_CSV = "text/csv";
	const CONTENT_TYPE_XML = "application/xml";
	
	/**
	 * Content type
	 * @var \Econda\ProductFeed\ContentType\ContentTypeInterface
	 */
	protected $contentType;
	
	/**
	 * Array of fields in this data source
	 * @var \Econda\ProductFeed\Description\Field[]
	 */
	protected $fields = array();
	
	abstract public function getType();

	public function open($uri)
	{
		if(empty($this->writer)) {
			throw new RuntimeException("Could not open feed writer for writing. Writer is empty.");
		}
		return $this->writer->open($uri);
	}
}