<?php
namespace Econda\ProductFeed\Writer;

use Econda\ProductFeed\Exception\RuntimeException;

/**
 * Base class for writers that create CSV files. Functions here are output format
 * specific but NOT product/category/... specific.
 */
class AbstractCsvWriter extends AbstractWriter
{
	/**
	 * Column delimiter
	 * @var string
	 */
	protected $delimiter = "|";
	
	/**
	 * Enclose field values with...
	 * @var string
	 */
	protected $enclosure = '"';
	

	/**
	 * Write CSV header
	 * @param array $columns
	 * @throws RuntimeException
	 * @return \Econda\ProductFeed\Writer\AbstractCsvWriter
	 */
	public function writeNamedColumnsHeader($columns)
	{
		$columns = $this->forceUtf8($columns);
		if(!fputcsv($this->handle, $columns, $this->delimiter, '"')) {
			throw new RuntimeException("Could not write CSV column header.");
		}
		return $this;
	}
	
	/**
	 * Write array as csv row. Converts to UTF-8 if catalog info defines a different
	 * input charset
	 * 
	 * (non-PHPdoc)
	 * @see \Econda\ProductFeed\Writer\AbstractWriter::writeRecord()
	 */
	public function writeRecord($data)
	{
		$data = $this->valueToField($data);
		
		if(	fwrite($this->handle, implode($this->delimiter, $data)) === false
		 || fwrite($this->handle, "\n") === false
		) {
			throw new RuntimeException("Could not write CSV data.");
		}
		return $this;
	}
	
	/**
	 * Encode value or array of values for Cross Sell CSV
	 * INCLUDES configured enclosure and converts to UTF-8 if required
	 * @param string|array $value
	 * @return string|array
	 */
	public function valueToField($value)
	{
		if(is_array($value)) {
			$ret = array();
			foreach($value as $k => $v) {
				$ret[$k] = $this->valueToField($v);
			}
		} else {
			$ret = preg_replace("/\\s{1,}/", " ", $value);
			$ret = trim($ret);
			$ret = str_replace(
					$this->enclosure,
					$this->enclosure.$this->enclosure,
					$ret
				);
			$ret = $this->enclosure . $this->forceUtf8($ret) . $this->enclosure;
		}
		return $ret;
	}
}