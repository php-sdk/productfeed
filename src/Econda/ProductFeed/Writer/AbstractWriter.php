<?php
namespace Econda\ProductFeed\Writer;

use Econda\ProductFeed\Catalog\CatalogInfo;
use Econda\Util\BaseObject;

/**
 * Base class that handles general writer functionality. Functions here must
 * not be output format or product/category specific!
 * 
 * @property string $inputCharset Charset of input data.
 * @property string $uri Output uri (file name or php://output, ...)
 */
abstract class AbstractWriter extends BaseObject
{
	/**
	 * Charset of incomming data. If not UTF-8, writer will convert to UTF-8
	 * @var string
	 */
	protected $inputCharset;
	
	/**
	 * fopen handle
	 * @var int
	 */
	protected $handle;
	
	/**
	 * Output URI
	 * @var string
	 */
	protected $uri;
	
	/**
	 * Check charset in catalog info and convert input to utf-8 if there's a different charset defined
	 * @param mixed $data array or string
	 * @return mixed
	 */
	public function forceUtf8($data)
	{
		if($this->inputCharset != 'UTF-8') {
			if(is_array($data)) {
				foreach($data as $key => $value) {
					$data[$key] = $this->forceUtf8($value);
				}
			} else {
				$data = utf8_encode($data);
			}
		}
		return $data;
	}
	
	/**
	 * Open URI
	 * @param string $uri
	 * @return \Econda\ProductFeed\Writer\AbstractCsvWriter
	 */
	public function open($uri = null)
	{
		if($uri != null) {
			$this->uri = $uri;
		}
		
		$this->handle = fopen($this->uri, 'w');
		return $this;
	}
	
	/**
	 * This abstract writer does not know how to write data, so you'll have to implement this
	 * method in an extending class
	 * @param array $data
	 */
	abstract public function writeRecord($data);
	
	/**
	 * Close output stream
	 * @return \Econda\ProductFeed\Writer\AbstractWriter
	 */
	public function close()
	{
		fclose($this->handle);
		return $this;
	}
}