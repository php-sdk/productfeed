<?php
namespace Econda\ProductFeed;

/**
 * FeedBuilder configuration object.
 * Provides methods to read, store and access config settings.
 * 
 * @property string $defaultLanguage
 * @property string[] $additionalLanguages
 * 
 * @author cluetjen
 */
class Config implements \ArrayAccess
{
	/**
	 * List of all configuration items
	 * @var array
	 */
	protected $data = array();

	public function __get($propertyName)
	{
		$key = preg_replace('/(?<=\\w)(?=[A-Z])/',"-$1", $propertyName);
		$key = strtolower($key);
		return $this->get($key);
	}

	/// Support for ArrayAccess Interface

	/**
	 * Support for isset($config['my-key'])
	 * @param mixed $index
	 * @return bool
	 */
	public function offsetExists($index)
	{
		return isset($this->data[$index]);
	}

	/**
	 * Support for $var = $config['my-key']
	 * @param mixed $index
	 * @return multitype:
	 */
	public function offsetGet($index)
	{
		return $this->data[$index];
	}

	/**
	 * Support for $config['my-key'] = 'value';
	 * @param mixed $index
	 * @param multitype $value
	 */
	public function offsetSet($index, $value)
	{
		$this->data[$index] = $value;
	}

	/**
	 * Support for unset($config['my-key']);
	 * @param mixed $index
	 */
	public function offsetUnset($index)
	{
		unset($this->data[$index]);
	}


	/**
	 * Get single config value by name
	 * @param string $name
	 * @return multitype:
	 */
	public function get($name)
	{
		return $this->data[$name];
	}

	/**
	 * Returns an array of all config settings
	 * @return multitype:
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Set single config value
	 * @param string $name
	 * @param multitype $value
	 */
	public function set($name, $value)
	{
		$this->data[$name] = $value;
	}

	/**
	 * Add configuration settings
	 * @param array $data
	 * @return \Econda\ProductFeed\FeedBuilder
	 */
	public function add($data, $value=null)
	{
		if(is_array($data) == false) {
			$this->assignArrayByPath($this->data, $data, $value);
		} else {
			foreach($data as $path => $value) {
				$this->assignArrayByPath($this->data, $path, $value);
			}
		}

		return $this;
	}

	/**
	 * Handles keys with dots as sub arrays
	 * @param array $arr
	 * @param string $path
	 * @param unknown $value
	 */
	private function assignArrayByPath(&$arr, $path, $value)
	{
		$keys = explode('.', $path);

		while ($key = array_shift($keys)) {
			$arr = &$arr[$key];
		}
		if(is_array($arr)) {
			$arr = array_merge($arr, is_array($value) ? $value : array($value));
		} else {
			$arr = $value;
		}
	}

	/**
	 * Read configuration from file and add to configuration property
	 * @param string $path path of config file
	 */
	public function readFile($path)
	{
		if(!file_exists($path)) {
			throw new \RuntimeException("Could not load configuration file from: $path");
		}
		$this->add(include $path);
		return $this;
	}

	/**
	 * Adds all files in given folder to configuration
	 * @param string $path
	 */
	public function readDir($path)
	{
		if(!file_exists($path)) {
			throw new \RuntimeException("Could not open configuration folder: $path");
		}
		foreach (new \DirectoryIterator($path) as $fileInfo) {
			if($fileInfo->isDot()) continue;
			if(strpos($fileInfo->getPathname(),".svn") > 0 ) continue;
			$this->readFile($fileInfo->getPathname());
		}
		return $this;
	}
}