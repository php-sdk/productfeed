<?php
namespace Econda\ProductFeed\Description;

use Econda\ProductFeed\AbstractFeed;
/**
 * Represents the product list. There must be one product list per feed.
 */
class ProductDataSource extends AbstractFeed
{
	public function getType()
	{
		return AbstractFeed::TYPE_PRODUCT_LIST;
	}
}