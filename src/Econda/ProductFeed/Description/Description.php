<?php
namespace Econda\ProductFeed\Description;
use Econda\Util\BaseObject;
use Econda\ProductFeed\AbstractFeed;

/**
 * Description of product feed (overall including all product and category feeds)
 * 
 * @property string $generatorName
 * @property string $generatorVersion
 * @property string $shopSystemName
 * @property string $shopSystemVersion
 * @property string $contactName
 * @property string $contactEmail
 * @property string $contactPhone
 * @property string $catalogCharset
 * @property string $catalogCurrency
 * 
 * @author cluetjen
 */
class Description extends BaseObject
{
	/**
	 * Name of generator, e.g. "econda top-shop plugin"
	 * @var string
	 */
	protected $generatorName = 'should be set in module config';
	
	/**
	 * Version number of generator, e.g. "1.5"
	 * @var string
	 */
	protected $generatorVersion = 'should be set in module config';
	
	/**
	 * Name of shop system
	 * @var string
	 */
	protected $shopSystemName = 'should be provided in catalog info';
	
	/**
	 * Version of shop system
	 * @var string
	 */
	protected $shopSystemVersion = 'should be provided in catalog info';
	
	/**
	 * Shop internal charset
	 * @var string
	 */
	protected $catalogCharset = 'should be provide in catalog info';
	
	/**
	 * Currency used in catalog according to ISO 4217
	 * @var string
	 */
	protected $catalogCurrency = 'unknown';
	
	/**
	 * Name of contact person in case of technical problems
	 * @var string
	 */
	protected $contactName = '';
	
	/**
	 * Email of contact person
	 * @var string
	 */
	protected $contactEmail = '';
	
	/**
	 * Phone number of contact person
	 * @var string
	 */
	public $contactPhone = '';
	
	/**
	 * List of datasources provided by this product feed
	 * @var DataSource[]
	 */
	protected $dataSources = array();
	
	/**
	 * Add one or more data sources
	 * @param \Econda\ProductFeed\Description\DataSource|\Econda\ProductFeed\Description\DataSource[] $dataSource
	 * @throws \InvalidArgumentException
	 * @return \Econda\ProductFeed\Description
	 */
	public function addDataSource($dataSource)
	{
		$list = is_array($dataSource) ? $dataSource : array($dataSource);
		
		foreach($list as $ds) {
			if($ds instanceof AbstractFeed == false) {
				throw new \InvalidArgumentException("Argument is not a valid data source");
			}
			$this->dataSources[] = $ds;
		}
		
		return $this;
	}
	
	/**
	 * Remove all data source from description
	 * @return \Econda\ProductFeed\Description
	 */
	public function clearDataSources()
	{
		$this->dataSources = array();
		return $this;
	}
	
	/**
	 * Get list of data sources or empty array
	 * @return \Econda\ProductFeed\Description\DataSource[]
	 */
	public function getDataSources()
	{
		return $this->dataSources;
	}

    // functions to get data beloning together in a array
    public function getTechnicalInfos(){
        $technicalcontact= array();
        $technicalcontact["contactName"] =  $this->contactName;
        $technicalcontact["contactEmail"] =  $this->contactEmail;
        $technicalcontact["contactPhone"] =  $this->contactPhone;
        return $technicalcontact;
    }

    public function getShopInfos(){
        $shopinfo= array();
        $shopinfo["generator"]= $this->generatorName;
        $shopinfo["generatorVersion"]= $this->generatorVersion;
        $shopinfo["shopSystem"]= $this->shopSystemName;
        $shopinfo["shopVersion"]= $this->shopSystemVersion;
        $shopinfo["charset"]= $this->catalogCharset;
        $shopinfo["currency"]= $this->catalogCurrency;
        return $shopinfo;
    }



    public function getDataSourcesInfo(){
        $allDataSource = array();
        foreach ($this->dataSources as $ds) {
            if($ds instanceof ProductDataSource) {
                $allDataSource[] = $this->renderProductDataSource($ds);
            } else {
                $allDataSource[] = $this->renderCategoryDataSource($ds);
            }
        }
        return $allDataSource;
    }
    private function renderProductDataSource(ProductDataSource $dataSource)
    {
        $dsource = array();
        $dsource["uri"] = $dataSource->getUri();
        $dsource["type"] = $dataSource->getType();
        $dsource["contentType"] = $dataSource->getContentType();
        $dsource["fields"]= $this->renderFields($dataSource->getFields());
        return  $dsource;
    }

    private function renderCategoryDataSource(CategoryDataSource $dataSource)
    {
        $dsource = array();
        $dsource["uri"] = $dataSource->getUri();
        $dsource["type"] = $dataSource->getType();
        $dsource["contentType"] = $dataSource->getContentType();
        $dsource["fields"]= $this->renderFields($dataSource->getFields());
        return  $dsource;
    }

    public function getFields(){
        $retFields= array();
        foreach($fields as $field) {
            $retField=array();
            $retField["name"]=$field->name;
            $retField["type"]=$field->type;
            $retField["usage"]=$field->usage;
            $retField["variation"]=$field->variation;
            $options = array();
            foreach($field->options as $key => $value) {
                $options[] = $key . ': ' . $value;
            }
            $retField["options"] =join(", ", $options);
            $retFields[$field->name]=$retField;
        }
    }

    private function renderFields($fields)
    {
        $retFields=array();
        foreach($fields as $field) {
            $retField=array();
            $retField["name"]=$field->name;
            $retField["type"]=$field->type;
            $retField["usage"]=$field->usage;
            $retField["variation"]=$field->variation;
            $options = array();
            foreach($field->options as $key => $value) {
                $options[] = $key . ': ' . $value;
            }
            $retField["options"] =join(", ", $options);
            $retFields[$field->name]=$retField;
        }
        return $retFields;
    }



}