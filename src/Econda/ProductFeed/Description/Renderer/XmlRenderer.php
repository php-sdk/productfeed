<?php
namespace Econda\ProductFeed\Description\Renderer;

use Econda\ProductFeed\Description\Description;
use Econda\ProductFeed\Description\ProductDataSource;
use Econda\Util\BaseObject;
use Econda\ProductFeed\Description\CategoryDataSource;

class XmlRenderer extends BaseObject implements RendererInterface
{
    /**
     * Description data
     * @var \Econda\ProductFeed\Description\Description
     */
    protected $description;
    protected $dataObject;

    /**
     * Complete configuration data
     * @var array
     */
    protected $config;

    public function render(Description $description)
    {
        $this->description = $description;

        $this->dataObject = array();

        $this->renderHeader();
        $this->renderInfoColumn();
        $this->renderShopSettings();
        $this->renderDataSources();
        $this->renderFooter();
        return $this->array2xml($this->dataObject);
    }

    private function renderHeader()
    {
        $this->dataObject["type"] = "econdaProductFeed";
    }

    private function renderInfoColumn()
    {
        $this->dataObject["technicalContact"] = $this->description->getTechnicalInfos();
        $this->dataObject["shopInfo"] =  $this->description->getShopInfos();
    }



    private function renderShopSettings()
    {
        $configData = $this->config->getData();
        $ritit = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($configData));
        foreach ($ritit as $leafValue) {
            $keys = array();
            foreach (range(0, $ritit->getDepth()) as $depth) {
                $keys[] = $ritit->getSubIterator($depth)->key();
            }
            $name=join('.', $keys);
            $configData[ $name]=$leafValue;
        }
        $this->dataObject["configurationDetails"] = $configData;
    }

    private function renderDataSources()
    {
        $dataSources = $this->description->getDataSources();
        $allDataSource = array();
        foreach ($dataSources as $ds) {
            if($ds instanceof ProductDataSource) {
                $allDataSource[] = $this->renderProductDataSource($ds);
            } else {
                $allDataSource[] = $this->renderCategoryDataSource($ds);
            }
        }
        $this->dataObject["dataSources"] = $allDataSource;
    }

    private function renderProductDataSource(ProductDataSource $dataSource)
    {
        $dsource = array();
        $dsource["uri"] = $dataSource->getUri();
        $dsource["type"] = $dataSource->getType();
        $dsource["contentType"] = $dataSource->getContentType();
        $dsource["fields"]= $this->renderFields($dataSource->getFields());
        return  $dsource;
    }

    private function renderCategoryDataSource(CategoryDataSource $dataSource)
    {
        $dsource = array();
        $dsource["uri"] = $dataSource->getUri();
        $dsource["type"] = $dataSource->getType();
        $dsource["contentType"] = $dataSource->getContentType();
        $dsource["fields"]= $this->renderFields($dataSource->getFields());
        return  $dsource;
    }

    private function renderFields($fields)
    {
        $retFields=array();
        foreach($fields as $field) {
            $retField=array();
            $retField["name"]=$field->name;
            $retField["type"]=$field->type;
            $retField["usage"]=$field->usage;
            $retField["variation"]=$field->variation;
            $options = array();
            foreach($field->options as $key => $value) {
                $options[] = $key . ': ' . $value;
            }
            $retField["options"] =join(", ", $options);
            $retFields[$field->name]=$retField;
        }
        return $retFields;
    }

    private function renderFooter()
    {	}


    function array2xml($array, $xml = false){
        if($xml === false){
            $xml = new \SimpleXMLElement('<root></root>');
        }
        foreach($array as $key => $value){

            if(is_array($value)){
                //$this->array2xml($value, $xml->addChild($key));
                if(!is_numeric($key)){$this->array2xml($value, $xml->addChild($key));}else{$this->array2xml($value, $xml->addChild("key_". $key));}
            }else{
                //$xml->addChild($key, $value);
                //$value = "<![CDATA[" . $value . "]]>"; // doesnt work
                //
                // see http://de.selfhtml.org/xml/regeln/zeichen.htm
                $value = str_replace ("<","&lt;",$value);
                $value = str_replace (">","&gt;",$value);
                $value = str_replace ("&","&amp;",$value);
                $value = str_replace ("\"","&quot;",$value);
                $value = str_replace ("'","&apos;;",$value);
                //if(preg_match("/<|\&|\?/",$value ) == 1){$value = "<![CDATA[" . $value . "]]>";}  // doesnt work
                if(!is_numeric($key)){$xml->addChild($key, $value);}else {$xml->addChild("key_".$key, $value);}
            }
        }
        return $xml->asXML();
    }


}