<?php

namespace Econda\ProductFeed\Description\Renderer;

use Econda\ProductFeed\Description\Description;
use Econda\Util\BaseObject;
use Econda\ProductFeed\CategoryFeed\CategoryFeed;
use Econda\ProductFeed\ProductFeed\ProductFeed;

class HtmlRenderer extends BaseObject implements RendererInterface {

    /**
     * Description data
     * @var \Econda\ProductFeed\Description\Description
     */
    protected $description;

    /**
     * Complete configuration data
     * @var array
     */
    protected $config;

    public function render(Description $description) {
        $this->description = $description;

        $html = array("<html>");
        $html[] = $this->renderHeader();
        $html[] = $this->renderInfoColumn();
        $html[] = $this->renderDataSources();
        $html[] = $this->renderShopSettings();
        $html[] = "</html>";

        return join("", $html);
    }

    private function renderHeader() {
        return join("", array(
            "<head>",
            "<style>",
            'body { margin: 10px; padding: 0; }',
            'h2 {margin: 0;}',
            "*{font-family:monospace;} ",
            "table.datasource { margin-bottom: 10px; margin-right: 540px; display: block; padding: 10px; background-color: #FFFFEE; }",
            "td, th { padding: 2px 4px; text-align: left; } ",
            "table.fields{background-color: #DDDDDD; min-width:400px;}",
            "table.fields td {background-color: #F0F0F0; }",
            "th {font-weight: bold; }",
            "div.infobox{ width:500px;float:right;}",
            "div.infobox .content {background-color: #125282; padding:10px;}",
            "div.infobox .content * {color: #FFFFFF;}",
            "</style>",
            "</head>",
            "<body>"
        ));
    }

    private function renderInfoColumn() {
        $d = $this->description;

        return join("", array(
            '<div class="infobox">',
            "<h2>econda Product Feed</h2>",
            '<div class="content">',
            "<h3>Technical Contact</h3>",
            "<table>",
            "<tr><td>Contact Name:</td><td>" . htmlspecialchars($d->contactName) . "</td></tr>",
            "<tr><td>Contact Email:</td><td>" . htmlspecialchars($d->contactEmail) . "</td></tr>",
            "<tr><td>Contact Phone:</td><td>" . htmlspecialchars($d->contactPhone) . "</td></tr>",
            "</table>",
            "<h3>Shop Info</h3>",
            "<table>",
            '<tr><td>Generator:</td><td>' . htmlspecialchars($d->generatorName) . '</td></tr>',
            '<tr><td>Version:</td><td>' . htmlspecialchars($d->generatorVersion) . '</td></tr>',
            '<tr><td>Shop System:</td><td>' . htmlspecialchars($d->shopSystemName) . '</td></tr>',
            '<tr><td>Version:</td><td>' . htmlspecialchars($d->shopSystemVersion) . '</td></tr>',
            '<tr><td>Charset:</td><td>' . htmlspecialchars($d->catalogCharset) . '</td></tr>',
            '<tr><td>Currency:</td><td>' . htmlspecialchars($d->catalogCurrency) . '</td></tr>',
            "</table>",
// 				'<h3>Configuration</h3>',
// 				'<ul>',
// 					'<li><a href="?format=xml">XML Configuration</a></li>',
// 					'<li><a href="?format=json">JSON Configuration</a></li>',
// 				'</ul>',
            '</div></div>',
        ));
    }

    private function renderShopSettings() {
        $configData = $this->config->getData();

        $ritit = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($configData));
        $result = array();
        $result[] = '<h2>Configuration Details</h2>';
        $result[] = '<table><tr><th>Key</th><th>Value</th></tr>';
        foreach ($ritit as $leafValue) {

            $keys = array();
            foreach (range(0, $ritit->getDepth()) as $depth) {
                $keys[] = $ritit->getSubIterator($depth)->key();
            }
            $result[] = '<tr><td>' . join('.', $keys) . '</td><td>' . htmlspecialchars($leafValue) . '</td></tr>';
        }
        $result[] = '</table>';

        return join('', $result);
    }

    private function renderDataSources() {
        $dataSources = $this->description->getDataSources();

        $html = array();
        $html[] = "<h2>Data Sources</h2>";

        foreach ($dataSources as $ds) {
            if ($ds instanceof ProductFeed) {
                $html[] = $this->renderProductDataSource($ds);
            } else {
                $html[] = $this->renderCategoryDataSource($ds);
            }
        }

        return join("", $html);
    }

    private function renderProductDataSource(ProductFeed $dataSource) {
        $uri = $dataSource->getUri();

        $fieldReaderClassNames = array();
        foreach ($dataSource->getFieldReaders() as $fr) {
            $fieldReaderClassNames[] = get_class($fr);
        }

        return join("", array(
            '<table class="datasource">',
            "<tr><td>URI:</td><td><a target=\"_blank\" href=\"$uri\">$uri</a></td></tr>",
            "<tr><td>Type:</td><td>" . $dataSource->getType() . "</td></tr>",
            "<tr><td>Reader Class:</td><td>" . get_class($dataSource->getListReader()) . "</td></tr>",
            "<tr><td>Field Reader Classes:</td><td>" . join(', ', $fieldReaderClassNames) . "</td></tr>",
            "<tr><td>Content-Type:</td><td>" . $dataSource->getContentType() . "</td></tr>",
            "<tr><td>Fields</td><td>" . $this->renderFields($dataSource->getFields()) . "</td></tr>",
            "</table>",
        ));
    }

    private function renderCategoryDataSource(CategoryFeed $dataSource) {
        $uri = $dataSource->getUri();
        return join("", array(
            '<table class="datasource">',
            "<tr><td>URI:</td><td><a target=\"_blank\" href=\"$uri\">$uri</a></td></tr>",
            "<tr><td>Type:</td><td>" . $dataSource->getType() . "</td></tr>",
            "<tr><td>Reader Class:</td><td>" . get_class($dataSource->getReader()) . "</td></tr>",
            "<tr><td>Field Reader Class:</td><td>" . get_class($dataSource->getReader()->getProductFieldReader()) . "</td></tr>",
            "<tr><td>Content-Type:</td><td>" . $dataSource->getContentType() . "</td></tr>",
            "<tr><td>Fields</td><td>" . $this->renderFields($dataSource->getFields()) . "</td></tr>",
            "</table>",
        ));
    }

    private function renderFields($fields) {
        $html = array();
        $html[] = '<table class="fields"><tr><th>Name</th><th>Type</th><th>Usage</th><th>Variation</th><th>Options</th><th>Description</td></tr>';
        foreach ($fields as $field) {
            $fieldInfo = "<tr><td>$field->name</td><td>$field->type</td><td>$field->usage</td><td>$field->variation</td><td>";
            $options = array();
            foreach ($field->options as $key => $value) {
                $options[] = $key . ': ' . $value;
            }
            $fieldInfo.= join(", ", $options) . "</td>";
            $fieldInfo.= "<td>" . htmlspecialchars($field->description) . "</td></tr>";
            $html[] = $fieldInfo;
        }
        $html[] = '</table>';

        return join("", $html);
    }

    private function renderFooter() {
        return "</body>";
    }

}
