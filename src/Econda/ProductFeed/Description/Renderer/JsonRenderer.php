<?php
namespace Econda\ProductFeed\Description\Renderer;

use Econda\ProductFeed\Description\Description;
use Econda\ProductFeed\Description\ProductDataSource;
use Econda\Util\BaseObject;
use Econda\ProductFeed\Description\CategoryDataSource;

class JsonRenderer extends BaseObject implements RendererInterface
{
    /**
     * Description data
     * @var \Econda\ProductFeed\Description\Description
     */
    protected $description;
    protected $jsonObject;

    /**
     * Complete configuration data
     * @var array
     */
    protected $config;

    public function render(Description $description)
    {
        $this->description = $description;
        $this->jsonObject = array();
        $this->renderHeader();
        $this->renderInfoColumn();
        $this->renderShopSettings();
        $this->renderDataSources();
        return json_encode($this->jsonObject);
    }

    private function renderHeader()
    {
        $this->jsonObject["type"] = "econdaProductFeed";
    }

    private function renderInfoColumn()
    {
        $this->jsonObject["technicalContact"] =$this->description->getTechnicalInfos();
        $this->jsonObject["shopInfo"] =  $this->description->getShopInfos();
    }



    private function renderShopSettings()
    {
        $configDatas = array();
        $configData = $this->config->getData();
        $ritit = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($configData));
        foreach ($ritit as $leafValue) {
            $keys = array();
            foreach (range(0, $ritit->getDepth()) as $depth) {
                $keys[] = $ritit->getSubIterator($depth)->key();
            }
            $name=join('.', $keys);
            $configDatas[ $name]=$leafValue;
        }
        $this->jsonObject["configurationDetails"] = $configDatas;
    }

    private function renderDataSources()
    {
        $this->jsonObject["dataSources"] = $this->description->getDataSourcesInfo();
    }

    private function renderFooter()
    {
    }
}