<?php
namespace Econda\ProductFeed\Description\Renderer;

use Econda\ProductFeed\Description\Description;

interface RendererInterface
{
	public function render(Description $description);
}