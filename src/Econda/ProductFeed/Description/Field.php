<?php
namespace Econda\ProductFeed\Description;
use \Econda\Util\BaseObject;

/**
 * A single field in a product or category data source description.
 * 
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $usage
 * @property string $variation
 * 
 * @author cluetjen
 */
class Field extends BaseObject
{
	const TYPE_INTEGER = "integer";
	const TYPE_STRING = "string";
	const TYPE_FLOAT = "float";
	const TYPE_BOOLEAN = "boolean";
        const TYPE_DATE = 'date';
	
	/**
	 * Field name used in data source
	 * @var string
	 */
	protected $name;
	
	/**
	 * Data type of field
	 * @var string
	 */
	protected $type = self::TYPE_STRING;
	
	const USAGE_CUSTOM = "custom";
	const USAGE_CATEGORY_ID = "category-id";
	const USAGE_CATEGORY_NAME = "category-name";
	const USAGE_CATEGORY_PARENT = "category-parent";
	const USAGE_PRODUCT_ID = "product-id";
	const USAGE_PRODUCT_NAME = "product-name";
	const USAGE_PRODUCT_DESCRIPTION = "product-description";
	const USAGE_PRODUCT_IMAGEURL = "product-imageurl";
	const USAGE_PRODUCT_URL = 'product-url';
	const USAGE_PRICE = "product-price";
	const USAGE_IS_LOWEST_PRICE = "product-is-lowest-price";
	const USAGE_RRP = "product-rrp";
	const USAGE_BRAND_NAMES = "product-brand-names";
	const USAGE_STOCK = 'product-stock';
	const USAGE_EAN = 'ean';
	
	/**
	 * Defines, how this field should be used in cross sell
	 * @var string
	 */
	protected $usage = self::USAGE_CUSTOM;
	
	const OPTION_RESOLUTION = "resolution";
	
	/**
	 * Variation name or "default"
	 * @var string
	 */
	protected $variation = 'default';
	
	/**
	 * Assoc array with field options
	 * @var array
	 */
	protected $options = array();
	
        /**
         * Description of field content
         * @var string
         */
        protected $description = '';
        
	public function setVariation($variation)
	{
		if(!$variation) {
			$this->variation = 'default';
		} else {
			$this->variation = $variation;
		}
		return $this;
	}
}