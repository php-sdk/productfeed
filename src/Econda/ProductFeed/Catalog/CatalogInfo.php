<?php

namespace Econda\ProductFeed\Catalog;

use Econda\Util\BaseObject;

/**
 * Provides information about the local product catalog. This is NOT identical to what we would
 * expect in a product feed etc...
 * 
 * @property string $shopSystemName
 * @property string $shopSystemVersion
 * @property string $charset
 * @property string $currency
 * @property string $defaultLanguage
 * @property string[] $additionalLanguages
 * 
 * @author cluetjen
 */
class CatalogInfo extends BaseObject {

    /**
     * Name of shop system
     * @var string
     */
    protected $shopSystemName = '';

    /**
     * Version of shop system
     * @var string
     */
    protected $shopSystemVersion = '';

    /**
     * Charset used in data (UTF-8, or ISO code)
     * @var string
     */
    protected $charset = 'UTF-8';

    /**
     * Currency used in shop according to ISO 4217
     * See http://en.wikipedia.org/wiki/ISO_4217
     * @var String
     */
    protected $currency = null;

    /**
     * Collection of available fields (meta data)
     * @var unknown
     */
    protected $fields;

    /**
     * Language array key => key, name => name, id => internalId of default language
     * @var string
     */
    protected $defaultLanguage;

    /**
     * Array of additional language key => array s. defaultLanguage
     * @var array
     */
    protected $additionalLanguages = array();

    /**
     * There must be a default language that will be used if no language is specified
     * 
     * @param string $languageKey ISO language code
     * @param string $displayName Display name
     * @param string $internalId  Shop internal language id
     * @return \Econda\Catalog\CatalogInfo
     */
    public function setDefaultLanguage($languageKey, $displayName = null, $internalId = null) {
        if (is_array($languageKey)) {
            $this->defaultLanguage = $languageKey;
        } else {
            $this->defaultLanguage = array(
                'key' => $languageKey,
                'name' => $displayName,
                'id' => $internalId,
                'variant' => null);
        }
        return $this;
    }

    /**
     * Add additional language
     * 
     * @param string $languageKey
     * @param string $displayName
     * @param string $internalId
     * @return \Econda\Catalog\CatalogInfo
     */
    public function addAdditionalLanguage($languageKey, $displayName, $internalId = null) {
        $this->additionalLanguages[$languageKey] = array(
            'key' => $languageKey,
            'name' => $displayName,
            'id' => $internalId,
            'variant' => $languageKey);
        return $this;
    }

}
