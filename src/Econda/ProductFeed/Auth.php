<?php
namespace Econda\ProductFeed;

use Econda\Util\BaseObject;

/**
 * Simple authentification class.
 * 
 * @author cluetjen
 */
class Auth extends BaseObject
{
	const DEFAULT_API_KEY = 'CHANGE-ME';
	
	protected $apiKey;
	
	/**
	 * True if password as read from config file is our default password
	 * @return boolean
	 */
	public function isDefaultApiKey()
	{
		return ($this->apiKey == self::DEFAULT_API_KEY); 
	}
	
	/**
	 * Checks if given api key is the same as the api key defined in configuration
	 * @param string $apiKey
	 * @return boolean
	 */
	public function isValidApiKey($apiKey)
	{
		return ($this->apiKey === $apiKey);
	}
	
	/**
	 * Check for valid basic auth credentials. Will stop script execution if no valid login available.
	 */
	public function requireValidBasicAuth()
	{
		if($this->isDefaultApiKey()) {
			$this->dieWithForbiddenResponse("Using the default api-key is not allowed. Change api key in feed configuration.");
		}
		
		// set http auth headers for apache+php-cgi work around
		if (isset($_SERVER['HTTP_AUTHORIZATION']) && preg_match('/Basic\s+(.*)$/i', $_SERVER['HTTP_AUTHORIZATION'], $matches)) {
			list($name, $password) = explode(':', base64_decode($matches[1]));
			$_SERVER['PHP_AUTH_USER'] = $name;
			$_SERVER['PHP_AUTH_PW'] = $password;
		}
		
		// set http auth headers for apache+php-cgi work around if variable gets renamed by apache
		if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) && preg_match('/Basic\s+(.*)$/i', $_SERVER['REDIRECT_HTTP_AUTHORIZATION'], $matches)) {
			list($name, $password) = explode(':', base64_decode($matches[1]));
			$_SERVER['PHP_AUTH_USER'] = $name;
			$_SERVER['PHP_AUTH_PW'] = $password;
		}
		
		// do auth
		if (!isset($_SERVER['PHP_AUTH_USER'])) {
			$this->dieWithAuthenticationRequiredResponse("Authentication required.");
			
		} else if($this->isValidApiKey($_SERVER['PHP_AUTH_PW']) === false) {
			$this->dieWithAuthenticationRequiredResponse("Wrong Username");
		}
	}
	
	/**
	 * Send 401 response headers and ask for basic auth.
	 * You should not use this function if inside of an mvc framework. Use response objects instead.
	 * @param string $message
	 */
	public function dieWithAuthenticationRequiredResponse($message)
	{
		header('WWW-Authenticate: Basic realm="econda product feed"');
		header('HTTP/1.0 401 Unauthorized');
		echo $message;
		die();
	}
	
	/**
	 * Send 403 response headers and display given message.
	 * You should not use this function if inside of an mvc framework. Use response objects instead.
	 * @param string $message
	 */
	public function dieWithForbiddenResponse($message)
	{
		header('HTTP/1.0 403 Forbidden');
		echo $message;
		die();
	}
}