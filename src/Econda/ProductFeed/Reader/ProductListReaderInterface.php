<?php
namespace Econda\ProductFeed\Reader;

use Econda\ProductFeed\ProductFeed\Product;
use Econda\ProductFeed\Config;
use Econda\ProductFeed\Catalog\CatalogInfo;

/**
 * Readers implementing this interface are responsible for creating a list of products to export.
 * 
 * * init() is the place to fetch a list of all products to export (e.g. do db queries here)
 * * we'll call getNextProduct() untill it returns false / null and expect a product as used in shop system or a db result row
 * * resolveProduct() is the place to split a product to multiple products (variants) or skip it
 * * initDto() will be called for each variant and must return an object of class ProductFeed\Product. Do not fill this object here. Use field readers to do this job.
 * 
 * @author cluetjen
 */
interface ProductListReaderInterface
{
	public function setConfig(Config $config);
	public function setCatalogInfo(CatalogInfo $catalogInfo);
	
	/**
	 * Will be called one time at the beginning of feed creation
	 */
	public function init();
	
	/**
	 * Return next product or false / null
	 */
	public function getNextProduct();

	/**
	 * Each item from the return value of getProductList() will be passed to this function.
	 * The return value must be a list of products. This way, you can return variants instead of
	 * the original product.
	 * @param mixed $product
	 * @return array
	 */
	public function resolveProduct($product);
	
	/**
	 * Initialize product, must return a Product object but must NOT fill any fields.
	 * Use field readers to fill the product object.
	 * 
	 * @param \Econda\ProductFeed\ProductFeed\Product $product
	 * @return Product
	 */
	public function initDto($product);
}