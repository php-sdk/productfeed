<?php
namespace Econda\ProductFeed\Reader;

use Econda\ProductFeed\FeedBuilder;
use Econda\ProductFeed\Config;

/**
 * A shop reader is responsible for providing basic information about the shop itself. The instance
 * will be passed to all other readers (product list, product fields, categories, ...)
 * 
 * @author cluetjen
 */
interface ShopReaderInterface
{
	/**
	 * @param Config $config
	 */
	public function setConfig(Config $config);
	
	/**
	 * Must return an initialized catalog info object
	 */
	public function getCatalogInfo();
}