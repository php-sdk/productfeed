<?php
namespace Econda\ProductFeed\Reader;

use Econda\ProductFeed\CategoryFeed\Category;
use Econda\ProductFeed\Config;
use Econda\ProductFeed\Catalog\CatalogInfo;

/**
 * Provides a reader to create a category feed.
 * 
 * @author cluetjen
 */
interface CategoryListReaderInterface
{
	public function setConfig(Config $config);
	public function setCatalogInfo(CatalogInfo $catalogInfo);
	
        /**
         * Called one time at the beginning of export
         */
        public function init();
        
	/**
	 * Must return a reader class to fill the corresponding field in product list
	 */
	public function getProductFieldReader();
	
	/**
	 * We'll call this function until it returns false / null.
	 */
	public function getNextCategory();
	
	/**
	 * Must return a product feed category object
	 * 
	 * @param unknown $categoryObject
	 * @return Category
	 */
	public function read($categoryObject);
}