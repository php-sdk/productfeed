<?php
namespace Econda\ProductFeed\Reader;

use Econda\ProductFeed\Description\Field;
use Econda\ProductFeed\ProductFeed\Product;
use Econda\ProductFeed\Config;
use Econda\ProductFeed\Catalog\CatalogInfo;

/**
 * A feed can use multiple product field readers. Each field readers must provide a set product fields.
 * 
 * @author cluetjen
 */
interface ProductFieldReaderInterface
{
	public function setConfig(Config $config);
	public function setCatalogInfo(CatalogInfo $catalogInfo);
	
	public function init();
	
	/**
	 * This function must return an array of Field objects describing the fields provided by this reader
	 * @return Field[]
	 */
	public function getProvidedFields();
	public function readProduct(Product $feedProductObject, $shopArticleObject);
}