<?php
namespace Econda\ProductFeed\ProductFeed;

/**
 * Data of one single product. Fill this object and add to product feed.
 */
class Product
{
	protected $fields = array();
	
	public function set($fieldName, $value, $variant=null)
	{
		if(!isset($this->fields[$fieldName])) {
			$this->fields[$fieldName] = array('value' => null, 'variants' => array());
		}
		
		if($variant == null) {
			$this->fields[$fieldName]['value'] = $value;
		} else {
			$this->fields[$fieldName]['variants'][$variant] = $value;
		}
		return $this;
	}
	
	public function get($fieldName, $variant=null)
	{
		if(!isset($this->fields[$fieldName])) {
			return null;
		}
		
		if($variant == null) {
			return $this->fields[$fieldName]['value'];
		} else {
			if(!isset($this->fields[$fieldName]['variants'][$variant])) {
				return null;
			} else {
				return $this->fields[$fieldName]['variants'][$variant];
			}
		}
	}
	
	public function getAllFieldNames()
	{
		return array_keys($this->fields);
	}
	
	/**
	 * Get all variants for given field name
	 * @param string $fieldName
	 * @return array
	 */
	public function getVariants($fieldName)
	{
		if(!isset($this->fields[$fieldName])) {
			return array();
		} else {
			return $this->fields[$fieldName]['variants'];
		}
	}
}