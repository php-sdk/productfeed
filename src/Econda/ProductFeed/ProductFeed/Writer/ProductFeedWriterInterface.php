<?php
namespace Econda\ProductFeed\ProductFeed\Writer;

use Econda\ProductFeed\ProductFeed\Product;

use Econda\ProductFeed\Catalog\CatalogInfo;

/**
 * This interface describes, which methods are required for a product feed writer class
 */
interface ProductFeedWriterInterface
{
	public function open($uri=null);
	public function setProductFields($fieldNames);
	public function writeHeader();
 	public function writeProduct(Product $product);
	public function writeFooter();
	public function close();
}