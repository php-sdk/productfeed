<?php

namespace Econda\ProductFeed\ProductFeed\Writer;

use Econda\ProductFeed\Exception\RuntimeException;
use Econda\ProductFeed\ProductFeed\Product;
use Econda\ProductFeed\Writer\AbstractCsvWriter;
use Econda\ProductFeed\Description\Field;

/**
 * This class knows how to write catalog info and product data to csv. It must not
 * contain general output specific functions (that's part of the abstract csv writer)
 * or not csv specific validation (that's part of the product feed). 
 */
class CsvWriter extends AbstractCsvWriter implements ProductFeedWriterInterface {

    /**
     * Array of field description objects
     * @var Field
     */
    protected $productFields;

    public function setProductFields($fieldNames) {
        $this->productFields = $fieldNames;
    }

    public function writeHeader() {
        if (empty($this->productFields)) {
            throw new RuntimeException("Product fields list is empty.");
        }
        $fieldNames = array();
        foreach ($this->productFields as $f) { /* @var $f Field */
            if ($f->variation != 'default') {
                $fieldNames[] = $f->name . '_' . $f->variation;
            } else {
                $fieldNames[] = $f->name;
            }
        }
        fputcsv($this->handle, $fieldNames, $this->delimiter, '"');
    }

    /**
     * This method defines how a product object should be transformed to csv
     * (non-PHPdoc)
     * @see \Econda\ProductFeed\ProductFeed\Writer\ProductFeedWriterInterface::writeProduct()
     */
    public function writeProduct(Product $product) {
        $data = array();

        foreach ($this->productFields as $field) {
            $fieldName = $field->getName();
            $value = $product->get($fieldName);
            if (is_array($value)) {
                $data[] = implode('^^', $value);
            } else {
                $data[] = $value;
            }
            foreach ($product->getVariants($fieldName) as $variantValue) {
                $fieldCount++;
                $data[] = $variantValue;
            }
        }
        $this->writeRecord($data);
    }

    public function writeFooter() {
        // nothing to do here for CSV
    }

}
