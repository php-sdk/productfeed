<?php

namespace Econda\ProductFeed\ProductFeed;

use Econda\ProductFeed\AbstractFeed;
use Econda\ProductFeed\ProductFeed\Writer\CsvWriter;
use Econda\ProductFeed\ProductFeed\Writer\ProductFeedWriterInterface;
use Econda\ProductFeed\Reader\ProductListReaderInterface;

/**
 * A product feed for cross sell
 * @property ProductListReaderInterface $listReader
 * @property ProductFieldReaderInterface[] $fieldReaders
 */
class ProductFeed extends AbstractFeed {

    protected $listReader;
    protected $fieldReaders;

    public function __construct($arguments = null) {
        $this->fieldReaders = array();
        $this->writer = new CsvWriter();

        parent::__construct($arguments);
    }

    /**
     * Array of fields that this feed contains.
     * @return multitype:
     */
    public function getFields() {
        $fields = array();
        foreach ($this->fieldReaders as $fr) { /* @var $fr ProductFieldReaderInterface */
            $fields = array_merge($fields, $fr->getProvidedFields());
        }
        return $fields;
    }

    /**
     * Clear existing field readers and add provided.
     * @param unknown $readers
     */
    public function setFieldReaders($readers) {
        $this->fieldReaders = array();
        $this->addFieldReader($readers);
    }

    /**
     * Add one or more field readers.
     * @param string|ProductFieldReaderInterface $reader
     * @return \Econda\ProductFeed\ProductFeed\ProductFeed
     */
    public function addFieldReader($reader) {
        if (is_array($reader)) {
            foreach ($reader as $r) {
                $this->addFieldReader($r);
            }
        } else {
            if (is_string($reader)) {
                $this->fieldReaders[] = new $reader();
            } else {
                $this->fieldReaders[] = $reader;
            }
        }
        return $this;
    }

    /**
     * Set product list reader
     * @param ProductListReaderInterface $reader
     * @return \Econda\ProductFeed\ProductFeed\ProductFeed
     */
    public function setListReader($reader) {
        if (is_string($reader)) {
            $this->listReader = new $reader();
        } else {
            $this->listReader = $reader;
        }
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see \Econda\ProductFeed\AbstractFeed::getType()
     */
    public function getType() {
        return self::TYPE_PRODUCT_LIST;
    }

    /**
     * Initialize product feed. Do only things that are required for all available actions here.
     * Do NOT start the export here, this function will be called for the overview too.
     */
    public function init() {
        $this->writer->inputCharset = $this->catalogInfo->charset;
        
        $this->listReader->setConfig($this->config);
        $this->listReader->setCatalogInfo($this->catalogInfo);

        $fieldReaders = $this->getFieldReaders();
        foreach ($fieldReaders as $fr) { /* @var $fr ProductFieldReaderInterface */
            $fr->setConfig($this->config);
            $fr->setCatalogInfo($this->catalogInfo);
        }
    }

    /**
     * Generate feed.
     */
    public function writeAll() {
        $writer = $this->getWriter(); /* @var $writer ProductFeedWriterInterface */
        $writer->setProductFields($this->getFields());

        $listReader = $this->getListReader(); /* @var $listReader ProductListReaderInterface */
        $listReader->init();

        $fieldReaders = $this->getFieldReaders();
        foreach ($fieldReaders as $fr) { /* @var $fr ProductFieldReaderInterface */
            $fr->init();
        }
        
        $writer->writeHeader();
        while ($srcItem = $listReader->getNextProduct()) {
            $variants = $listReader->resolveProduct($srcItem);
            foreach ($variants as $srcVariant) {
                $destItem = $listReader->initDto($srcVariant);
                foreach ($fieldReaders as $fr) { /* @var $fr ProductFieldReaderInterface */
                    $fr->readProduct($destItem, $srcVariant);
                }
                $writer->writeProduct($destItem);
                unset($destItem);
            }
            unset($variants);
        }
    }

    /**
     * Adds a single product to feed.
     * @param Product $product
     */
    public function writeProduct(Product $product) {
        $this->writer->writeProduct($product);
    }

}
