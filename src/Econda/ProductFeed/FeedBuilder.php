<?php

namespace Econda\ProductFeed;

use Econda\Catalog\CatalogInfo;
use Econda\ProductFeed\Description\Description;
use Econda\ProductFeed\Description\Renderer\HtmlRenderer;
use Econda\ProductFeed\Description\Renderer\JsonRenderer;
use Econda\ProductFeed\Reader\ProductListReaderInterface;
use Econda\ProductFeed\Exception\RuntimeException;
use Econda\Util\BaseObject;
use Econda\ProductFeed\Reader\CategoryListReaderInterface;
use Econda\ProductFeed\CategoryFeed\CategoryFeed;
use Econda\ProductFeed\CategoryFeed\Writer\CsvWriter;
use Econda\ProductFeed\ContentType\Csv;
use Econda\ProductFeed\ProductFeed\ProductFeed;
use Econda\ProductFeed\Description\Renderer\XmlRenderer;

/**
 * Feed builder base class
 * 
 * @author cluetjen
 */
class FeedBuilder extends BaseObject {

    /**
     * Configuration from config files
     * @var Config
     */
    protected $config;

    /**
     * Catalog info object. Must be filled by ShopReader
     * @var CatalogInfo
     */
    protected $catalogInfo;

    /**
     * Instance of Auth class
     * @var Auth
     */
    protected $auth;

    /**
     * Reader for basic shop data
     * @var ShopReaderInterface
     */
    protected $shopReader;

    /**
     * Product list reader instance
     * @var ProductFeed
     */
    protected $productDataSource;

    /**
     * (external) category list readers 
     * @var CategoryFeed
     */
    protected $categoryDataSources;

    /**
     * Content type to use for category lists
     * @var ContentTypeInterface
     */
    protected $categoryFeedContentType;

    /**
     * Content Type to use for product feed
     * @var ContentTypeInterface
     */
    protected $productFeedContentType;

    public function __construct($arguments = null) {
        
        $this->config = new Config();
        $this->categoryFeedContentType = new Csv(array());
        $this->productFeedContentType = new Csv(array());

        parent::__construct($arguments);
    }

    /**
     * This function must be called at the beginning of each reaquest.
     */
    public function init() {
        $this
                ->initAuth()
                ->initShopReader()
                ->initCategoryDataSources()
                ->initProductDataSource();
    }

    /**
     * Initializes auth
     * @throws RuntimeException
     * @return \Econda\ProductFeed\FeedBuilder
     */
    private function initAuth() {
        if (empty($this->config['auth'])) {
            throw new RuntimeException('Missing auth configuration.');
        }
        $this->auth = new Auth($this->config['auth']);

        return $this;
    }

    /**
     * Initializes the shop reader that provides common information about the shop system and settings.
     * 
     * @throws RuntimeException
     * @return \Econda\ProductFeed\FeedBuilder
     */
    private function initShopReader() {
        if (empty($this->config['shop-reader-class'])) {
            throw new RuntimeException('No shop reader class found in confiugration. Set "shop-reader-class" config option.');
        }
        $shopReaderClass = $this->config['shop-reader-class'];
        $this->shopReader = new $shopReaderClass();
        $this->shopReader->setConfig($this->config);

        $this->catalogInfo = $this->shopReader->getCatalogInfo();

        if (!isset($this->config['default-language'])) {
            $this->config['default-language'] = $this->catalogInfo->defaultLanguage;
        }
        if (!isset($this->config['additional-languages'])) {
            $this->config['additional-languages'] = $this->catalogInfo->additionalLanguages;
        }

        return $this;
    }

    /**
     * Setup all category data feeds including their readers.
     * 
     * @throws RuntimeException
     * @return \Econda\ProductFeed\FeedBuilder
     */
    private function initCategoryDataSources() {
        if (isset($this->config['category-reader-classes']) === false) {
            throw new RuntimeException('No category reader classes found in confiugration. Set "category-reader-classes" config option.');
        }

        $categoryUrlTemplate = $this->getModuleBaseUrl() . '&type=category&categoryFeedIndex=%d';
        $this->categoryDataSources = array();
        $i = 0;
        foreach ($this->config['category-reader-classes'] as $className) {
            $dataSource = new CategoryFeed(array(
                'readerClassName' => $className,
                'uri' => sprintf($categoryUrlTemplate, $i++),
                'contentType' => $this->categoryFeedContentType,
                'config' => $this->config,
                'catalogInfo' => $this->catalogInfo,
            ));
            $dataSource->init();
            $this->categoryDataSources[] = $dataSource;
        }

        return $this;
    }

    /**
     * Initialize the product feed including field readers...
     * 
     * @throws RuntimeException
     * @return \Econda\ProductFeed\FeedBuilder
     */
    private function initProductDataSource() {
        if (empty($this->config['product-list-reader-class'])) {
            throw new RuntimeException('No product list reader class found in confiugration. Set "product-list-reader-class" config option.');
        }
        if (empty($this->config['product-field-reader-classes'])) {
            throw new RuntimeException('No product field reader classes found in confiugration. Set "product-field-reader-classes" config option.');
        }
        $fieldReaders = $this->config['product-field-reader-classes'];
        foreach ($this->categoryDataSources as $cd) { /* @var $cd CategoryDataSource */
            $fieldReaders[] = $cd->getReader()->getProductFieldReader();
        }

        $listReader = $this->config['product-list-reader-class'];

        $this->productDataSource = new ProductFeed(array(
            'listReader' => $listReader,
            'fieldReaders' => $fieldReaders,
            'uri' => $this->getModuleBaseUrl() . '&type=product',
            'contentType' => $this->categoryFeedContentType,
            'config' => $this->config,
            'catalogInfo' => $this->catalogInfo,
        ));
        $this->productDataSource->init();

        return $this;
    }

    /**
     * Writes html description of feed
     */
    public function writeDescription($out, $contentType="html") {

        if(isset($this->catalogInfo) === false) {
            throw new RuntimeException('Cannot render description. Catalog info not available.');
        }
        
        $description = new Description();
        $description->generatorName = $this->config['module-name'];
        $description->generatorVersion = $this->config['module-version'];
        $description->contactName = $this->config['contact-name'];
        $description->contactEmail = $this->config['contact-email'];
        $description->contactPhone = $this->config['contact-phone'];
        $description->shopSystemName = $this->catalogInfo->shopSystemName;
        $description->shopSystemVersion = $this->catalogInfo->shopSystemVersion;
        $description->catalogCharset = $this->catalogInfo->charset;
        $description->catalogCurrency = $this->catalogInfo->currency;

        // add category data sources
        $description->addDataSource($this->categoryDataSources);

        // add product data source
        $description->addDataSource($this->productDataSource);

        switch ($contentType) {
            case 'html':
                $renderer = new HtmlRenderer(array(
                    'config' => $this->config
                ));
                break;
            case 'json':
                $renderer = new JsonRenderer(array(
                    'config' => $this->config
                ));
                break;
            case 'xml':
                $renderer = new XmlRenderer(array(
                    'config' => $this->config
                ));
                break;
            default:
                throw new RuntimeException("No renderer available for content type: " . $this->descriptionFeedContentType);
        }
        file_put_contents($out, $renderer->render($description));
    }

    public function protect() {
        $this->auth->requireValidBasicAuth();
    }
    
    /**
     * Write product feed to given file handle
     */
    public function writeProductFeed($uri) {
        $feed = $this->productDataSource;
        $feed->open($uri);
        $feed->writeAll();
    }

    /**
     * Write category feed with given index to given file handle
     * @param int $fileHandle
     * @param int $index
     */
    public function writeCategoryFeed($uri, $index) {
        if ($index >= count($this->categoryDataSources)) {
            throw new \InvalidArgumentException('Invalid category index.');
        }
        $feed = $this->categoryDataSources[$index]; /* @var CategoryListReaderInterface */
        $feed->open($uri);
        $feed->writeAll();
    }

    private function getModuleBaseUrl() {
        $uri = $_SERVER['REQUEST_URI'];
        if (strpos($uri, '?') === false) {
            $uri .= '?debug=false';
        }
        return $uri;
    }

}
