econda/productfeed
===============

Base classes to generate product feeds. Use shop system specific projects for details.

### Installing via Composer

The recommended way to install this library is through [Composer](http://getcomposer.org).


```bash
# Install Composer
curl -sS https://getcomposer.org/installer | php
```

After installing, you need to require Composer's autoloader:

```php
require 'vendor/autoload.php';
```

### Documentation
For further documentation, please visit our support portal https://support.econda.de
